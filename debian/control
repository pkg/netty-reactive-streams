Source: netty-reactive-streams
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 libnetty-java (>= 1:4.1.13),
 libmaven-bundle-plugin-java,
 libreactive-streams-java,
 maven-debian-helper (>= 2.1)
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/java-team/netty-reactive-streams.git
Vcs-Browser: https://salsa.debian.org/java-team/netty-reactive-streams
Homepage: https://github.com/playframework/netty-reactive-streams

Package: libnetty-reactive-streams-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: Netty Reactive Streams
 This library provides a reactive streams implementation for Netty.
 Essentially it comes in the form of two channel handlers, one that
 publishes inbound messages received on a channel to a Publisher,
 and another that writes messages received by a Subscriber outbound.
 .
 Features include:
  * Full backpressure support, as long as the AUTO_READ channel option
    is disabled.
  * Publishers/subscribers can be dynamically added and removed from
    the pipeline.
  * Multiple publishers/subscribers can be inserted into the pipeline.
  * Customisable cancel/complete/failure handling.
